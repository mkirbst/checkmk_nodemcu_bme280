In this folder is the stuff for the Check_MK instance. I have choosen to use my \
name in the plugin to avoid name conflicts with existing plugins while debugging.\
Be careful while handling python files, in python source code structure is partly\
done via whitespaces. What you have to do ? You have to put the two python files\
to your Check_MK instance. For example if your monitoring site has the name MYSITE,\
then the files have to be placed here:

```
/omd/sites/MYSITE/local/share/check_mk/checks/nodemcu_mkirbst

/opt/omd/sites/MYSITE/local/share/check_mk/web/plugins/perfometer/nodemcu_mkirbst.py
```


After you placed the files there, go to WATO, add new host and do full service discovery.
Now all services should be discovered like in the screenshot. If not, check the files
(file encoding, unix formatted, file permissions, all the standard stuff, you know ?)