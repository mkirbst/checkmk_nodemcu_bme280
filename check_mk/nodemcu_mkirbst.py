#!/usr/bin/python
# -*- encoding: utf-8; py-indent-offset: 4 -*-
# file location: /opt/omd/sites/sili/local/share/check_mk/web/plugins/perfometer/nodemcu_mkirbst.py

def perfometer_voltage(row, check_command, perf_data):
    color = "#808000"
    value = float(perf_data[0][1])
    return "%0.3f V" % value, perfometer_logarithmic(value, 12, 2, color)

perfometers["check_mk-nodemcu_mkirbst.vcc"] = perfometer_voltage

def perfometer_bme280_value_age(row, check_command, perf_data):
    color = "#ff0000"	# red
    value = int(perf_data[0][1])
    if value < 1000:
        color = "#009933"	# olive green
    elif value < 2000:
        color <= "#ff9900"	# orange

    return u"%d ms" % value, perfometer_logarithmic(value, 1000, 2, color)

perfometers["check_mk-nodemcu_mkirbst.bme280_value_age"] = perfometer_bme280_value_age

def perfometer_temperature(row, check_command, perf_data):
    color = "#39f"
    value = float(perf_data[0][1])
    return u"%.1f °C" % value, perfometer_logarithmic(value, 40, 1.2, color)

perfometers["check_mk-nodemcu_mkirbst.bme280_temp"] = perfometer_temperature


def perfometer_pressure(row, check_command, perf_data):
    pressure = float(perf_data[0][1])
    return "%d hPa" % pressure, perfometer_logarithmic(pressure, 1, 2, "#da6")

perfometers['check_mk-nodemcu_mkirbst.bme280_pres'] = perfometer_pressure


def perfometer_humidity(row, check_command, perf_data):
    humidity = float(perf_data[0][1])
    return "%d % %" % humidity, perfometer_linear(humidity, '#6f2')

perfometers['check_mk-nodemcu_mkirbst.bme280_rhum'] = perfometer_humidity


def perfometer_nodemcu_mkirbst_cpu_freq(row, check_command, perf_data):
    color = "#39f"
    value = int(perf_data[0][1])
    valuePercent = value * 0.625
    return "%d MHz" % value, perfometer_linear(valuePercent, '#6f2')

perfometers["check_mk-nodemcu_mkirbst.cpu_freq"] = perfometer_nodemcu_mkirbst_cpu_freq

def perfometer_nodemcu_mkirbst_free_heap(row, check_command, perf_data):
    color = "#39f"
    value = int(perf_data[0][1])
    valuePercent = ( value * 100 ) / 50000
    return "%d Bytes" % value, perfometer_linear(valuePercent, '#6f2')

perfometers["check_mk-nodemcu_mkirbst.free_heap"] = perfometer_nodemcu_mkirbst_free_heap

def perfometer_nodemcu_mkirbst_max_free_block_size(row, check_command, perf_data):
    color = "#39f"
    value = int(perf_data[0][1])
    valuePercent = ( value * 100 ) / 50000
    return "%d Bytes" % value, perfometer_linear(valuePercent, '#6f2')

perfometers["check_mk-nodemcu_mkirbst.max_free_block_size"] = perfometer_nodemcu_mkirbst_max_free_block_size


def perfometer_nodemcu_mkirbst_rssi(row, check_command, perf_data):
    color = "#39f"
    value = int(perf_data[0][1])
    valuePercent = 0
    if value >= -50:
        valuePercent = 100
    elif value >= -100:
        valuePercent = 2 * ( value + 100 )

    return "%d dBm" % value, perfometer_linear(valuePercent, '#6f2')

perfometers["check_mk-nodemcu_mkirbst.rssi"] = perfometer_nodemcu_mkirbst_rssi

def perfometer_nodemcu_mkirbst_heap_frag(row, check_command, perf_data):
    heap_frag = float(perf_data[0][1])
    return "%d % %" % heap_frag, perfometer_linear(heap_frag, '#6f2')

perfometers['check_mk-nodemcu_mkirbst.heap_frag'] = perfometer_nodemcu_mkirbst_heap_frag
