#include <Wire.h>
#include <ACROBOTIC_SSD1306.h>
#include <BME280I2C.h>
#include <ESP8266WiFi.h>
//#include <WiFiUdp.h>

// telnet example https://www.rudiswiki.de/wiki9/WiFiTelnetServer
boolean debug = false;  // true = more messages
//boolean debug = true;

// LED is needed for failure signalling
const short int BUILTIN_LED2 = 16;  //GPIO16 on NodeMCU (ESP-12)

unsigned long startTime = millis();
// /telnet

// ENTER HERE YOUR WLAN SSID:
const char* ssid     = "chucknorris";
// ENTER HERE YOUR WLAN PASSWORD:
const char* password = "chuck_norris_need_no_WLAN_password-!!"; 

const String sHostname = "esp_test01";

const int OLED_SCREEN_WIDTH = 16;   // 7x5 -> 25, 8x8 -> 16

//#define BME280_I2C_ADDRESS  0x76
//Adafruit_BME280  bme280;
BME280I2C bme;


float fTemp = 1.0, fHum = 1.0, fPres = 1.0;               //global var for bme280 sensors -> just 1 update function for 3 values
unsigned long bme_last_read = 0;                    //bme counter valiable for grant delay of min 1 sec wait time betweens two readings

const int iScreen_time = 8000;                      // time to show each screen in ms 
int iAct_screen = 0;                                // screen counter variable
unsigned long ulLast_screen_change = 0;             // when was the last screen change

#define MAX_TELNET_CLIENTS 2

WiFiServer telnetServer(6556);
WiFiClient serverClient;

ADC_MODE(ADC_VCC);  // needed to measure Vcc

void setup()
{
  delay(2000);
  pinMode(LED_BUILTIN, OUTPUT);     // Initialize the LED_BUILTIN pin as an output
  //Serial.setDebugOutput(true);
  Serial.begin(115200);
  Serial.print("Chip ID: 0x");
  Serial.println(ESP.getChipId(), HEX);
  delay(2000);
  Wire.begin(0,2);
  delay(2000);  
  oled.init();                      // Initialze SSD1306 OLED display
  //oled.setFont(font5x7);
  oled.setFont(font8x8);
  oled.clearDisplay();              // Clear screen
              
  oled.setTextXY(0,((OLED_SCREEN_WIDTH - strlen("RAUMKLIMA"))/2));              // Set cursor position
  oled.putString("RAUMKLIMA");

  while(!bme.begin())
  {
    Serial.println("Could not find BME280 sensor!");
    oled.setTextXY(2,0);              // Set cursor position
    oled.putString("wait for bme280");
    oled.setTextXY(4,0);              // Set cursor position
    oled.putString((String)(millis()/1000));
    delay(1000);
  }

  // bme.chipID(); // Deprecated. See chipModel().
  switch(bme.chipModel())
  {
     case BME280::ChipModel_BME280:
       Serial.println("Found BME280 sensor! Success.");
       break;
     case BME280::ChipModel_BMP280:
       Serial.println("Found BMP280 sensor! No Humidity available.");
       break;
     default:
       Serial.println("Found UNKNOWN sensor! Error!");
  }
  oled.setTextXY(2,0);              // Set cursor position
  oled.putString("Starting up,");
  oled.setTextXY(4,0);              // Set cursor position
  oled.putString("please wait");

  oled.setTextXY(6,0);              // Set cursor position
  oled.putString("wait for wlan ..");
  WiFi.hostname(sHostname);
  WiFi.begin(ssid, password);
  if (debug == true) 
  {
    WiFi.printDiag(Serial);
  }
  delay(2000);
 
   /*
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
    oled.setTextXY(7,0);              // Set cursor position
    oled.putString((String)(millis()/1000)+"s");
  }
  */
  Serial.println();

  Serial.print("Connected, IP address: ");
  Serial.println(WiFi.localIP());

  delay(2000);
  oled.clearDisplay();              // Clear screen
  delay(2000);

//telnet
  telnetServer.begin();
  telnetServer.setNoDelay(true);
  //Serial.println("Please connect Telnet Client, exit with ^] and 'quit'");
  //Serial.print("Free Heap[B]: ");
  //Serial.println(ESP.getFreeHeap());
}

void loop()
{
  bme280updateGlobalVars();

  char str_temp[6];  
  char buf[17];

// telnet
// look for Client connect trial
  if (telnetServer.hasClient()) 
  {
    if (!serverClient || !serverClient.connected()) 
    {
      if (serverClient) 
      {
        serverClient.stop();
        //Serial.println("Telnet Client Stop");
      }
      serverClient = telnetServer.available();
      //Serial.println("New Telnet client");
      serverClient.flush();  // clear input buffer, else you get strange characters 
    }
  }
  /*
  while(serverClient.available()) 
  {  // get data from Client
    Serial.write(serverClient.read());
  }
 */
  if (millis() - startTime > 2000) 
  { // run every 2000 ms
    startTime = millis();
    
    if (serverClient && serverClient.connected()) 
    {  // send data to Client
      char buf[256];  
      serverClient.println("<<<check_mk>>>");
      serverClient.println("Version: nodemcu_mkirbst_20190910");
      serverClient.println("AgentOS: Arduino");
      serverClient.println("Hostname: " + (String)WiFi.hostname());
      serverClient.println("OnlyFrom:");
      serverClient.println("<<<uptime>>>");
      serverClient.println((String)(millis()/1000) + ".00 " + (String)(millis()/2000) + ".00 ");
      serverClient.println("<<<nodemcu_mkirbst>>>");
      // sensor_type sensor_number value_type value value_unit
      serverClient.print("free_heap ");
      serverClient.print(ESP.getFreeHeap());
      serverClient.println(" b");
      
      serverClient.print("heap_frag ");
      serverClient.print(ESP.getHeapFragmentation());
      serverClient.println(" %");

      serverClient.print("max_free_block_size ");
      serverClient.print(ESP.getMaxFreeBlockSize());
      serverClient.println(" b");
      
      
      float fVcc = ESP.getVcc()/1000.0;
      sprintf(buf, "Vcc %.2f V", fVcc );
      serverClient.println(buf);
      
      serverClient.print("last_restart_reason ");
      serverClient.println(ESP.getResetReason());
      
      serverClient.print("chip_id ");
      serverClient.println(ESP.getChipId());
      
      serverClient.print("core_version ");
      serverClient.println(ESP.getCoreVersion());
      
      serverClient.print("sdk_version ");
      serverClient.println(ESP.getSdkVersion());

      sprintf(buf, "cpu_freq %u MHz", (ESP.getCpuFreqMHz()) );
      serverClient.println(buf);
      
      sprintf(buf, "sketch_size %u b", ((uint32_t)ESP.getSketchSize()) );
      serverClient.println(buf);
      
      sprintf(buf, "free_sketch_space %u b", ((uint32_t)ESP.getFreeSketchSpace()) );
      serverClient.println(buf);

      serverClient.print("sketch_md5 ");
      serverClient.println(ESP.getSketchMD5());

      serverClient.print("flash_chip_id ");
      serverClient.println(ESP.getFlashChipId());

      serverClient.print("flash_chip_size ");
      serverClient.println(ESP.getFlashChipSize());

      serverClient.print("flash_chip_real_size ");
      serverClient.println(ESP.getFlashChipRealSize());

      sprintf(buf, "flash_chip_speed %u Hz", (ESP.getFlashChipSpeed()) );
      serverClient.println(buf);

      serverClient.print("cycle_count ");
      serverClient.println((uint32_t)ESP.getCycleCount());

      // wifi stats
      serverClient.print("rssi ");
      serverClient.print(WiFi.RSSI());
      serverClient.println(" dBm");

      serverClient.print("mac ");
      serverClient.println(WiFi.macAddress());
      
      serverClient.print("hostname ");
      serverClient.println(WiFi.hostname());
      
      serverClient.print("local_ip_v4 ");
      serverClient.println(WiFi.localIP());
      serverClient.print("gateway_v4 ");
      serverClient.println(WiFi.gatewayIP());
      serverClient.print("dns0_v4 ");
      serverClient.println(WiFi.dnsIP());
      serverClient.print("dns1_v4 ");
      serverClient.println(WiFi.dnsIP(1));
      serverClient.print("dns2_v4 ");
      serverClient.println(WiFi.dnsIP(2));
      serverClient.print("dns3_v4 ");
      serverClient.println(WiFi.dnsIP(3));
      serverClient.print("ssid ");
      serverClient.println(WiFi.SSID());
      serverClient.print("bssid ");
      serverClient.println(WiFi.BSSIDstr());
      
      //bme280
      sprintf(buf, "bme280 1 temp %.1f °C", fTemp);
      serverClient.println(buf);
      sprintf(buf, "bme280 1 rhum %d %%", (int)fHum);
      serverClient.println(buf);
      sprintf(buf, "bme280 1 pres %d hPa", (int)fPres);
      serverClient.println(buf);
      sprintf(buf, "bme280 1 value_age %u ms", abs( millis() - bme_last_read ) );
      serverClient.println(buf);
      
      
      
      
      serverClient.stop();
    }
  }
  delay(10); // to avoid strange characters left in buffer
// /telnet

  changeScreen();

  //reconnect idea: http://henrysbench.capnfatz.com/henrys-bench/arduino-projects-tips-and-more/connect-nodemcu-esp-12e-to-wifi-router-using-arduino-ide/
  int wifi_retry = 0;
  while(WiFi.status() != WL_CONNECTED && wifi_retry < 5 ) {
    wifi_retry++;
    int waittime_between_connect_tries = 120;
    /*
    oled.setTextXY(6,0);              // Set cursor position
    oled.putString("try:"+(String)wifi_retry);
    oled.putString((String)wifi_retry);
    */
    oled.setTextXY(6,0);              // Set cursor position
    oled.putString(((String)ssid).substring(0,15));
   
    Serial.println("WiFi not connected. Try to reconnect");
    WiFi.disconnect();
    delay(100);
    WiFi.mode(WIFI_OFF);
    delay(100);
    WiFi.mode(WIFI_STA);
    delay(100);
    
    WiFi.hostname(sHostname);
    WiFi.begin(ssid, password);
    if (debug == true) 
    {
      WiFi.printDiag(Serial);
    }
    while ( (WiFi.status() != WL_CONNECTED) && (waittime_between_connect_tries > 0)  )
    {
      delay(1000);
      Serial.print(".");
      oled.setTextXY(7,0);              // Set cursor position
      oled.putString("try " + (String)wifi_retry + " wait " + (String)(120-waittime_between_connect_tries)+"s");
      waittime_between_connect_tries--;
    }
  }
  if(wifi_retry >= 5)
  {
      Serial.println("\nReboot");
      ESP.restart();
  }
  else
  {
    //Serial.print("Connected, IP address: ");
    //Serial.println(WiFi.localIP());
  }
}

// every 3s change to next screen
/*  const int iScreen_time = 3000;                      // time to show each screen in ms 
    int iAct_screen = 0;                                // screen counter variable
    unsigned long ulLast_screen_change = 0;             // when was the last screen change
*/
void changeScreen()
{
  if (abs(millis() - ulLast_screen_change) >= iScreen_time)
  {
    iAct_screen++;
    ulLast_screen_change = millis();
  }
  switch (iAct_screen%3)
  {
    case 0:
      oled_show_screen_system(); 
      break;
    case 1:
      oled_show_screen_room(); 
      break;
    case 2:
      oled_show_screen_wifi(); 
      break;
    default:
      oled_show_screen_room();
      break;
  }
}

void oled_show_screen_system()
{
  
    //system 
  oled_show_row_cString_8x8x16(0, "SYSTEM STATUS");
  oled_empty_line_16(1);
  oled_show_row_hostname_8x8x16(2);
  oled_empty_line_16(3);
  oled_show_row_free_heap_8x8x16(4);
  oled_empty_line_16(5);
  oled_show_row_uptime_8x8x16(6);
  oled_empty_line_16(7);
  
}
void oled_show_screen_room()
{
  
  // room 
  oled_show_row_cString_8x8x16(0, "ROOM STATUS");
  oled_empty_line_16(1);
  oled_show_row_temp_8x8x16(2);
  oled_empty_line_16(3);
  oled_show_row_humi_8x8x16(4);
  oled_empty_line_16(5);
  oled_show_row_pres_8x8x16(6);
  oled_empty_line_16(7);
}
void oled_show_screen_wifi()
{
  
  //wifi
  oled_show_row_cString_8x8x16(0, "WIFI STATUS");
  oled_show_row_hostname_8x8x16(1);
  oled_show_row_lString_8x8x16(2, "client mac:");
  oled_show_row_mac_address_8x8x16(3);
  oled_show_row_lString_8x8x16(4, "client ip:");
  oled_show_row_ip_address_8x8x16(5);
  oled_show_row_ssid_8x8x16(6);
  oled_show_row_rssi_8x8x16(7);
}


void oled_empty_line_16(int iRow)
{
  for(int i = 0; i < 16; i++)
  {
    oled.setTextXY(iRow,i);
    oled.putChar(' ');
  }
}

void oled_show_row_lString_8x8x16(int iRow, String s)
{
  if (s.length() > 16) 
  {
    s = s.substring(0,15);
  }
  oled.setTextXY(iRow,0);              // Set cursor position
  oled.putString(s);
  for (int i = s.length(); i < OLED_SCREEN_WIDTH; i++)
  {
    oled.setTextXY(iRow, i);
    oled.putChar(' '); 
  }
}

void oled_show_row_cString_8x8x16(int iRow, String s)
{
  // cut off string if its to long
  if (s.length() > 16) 
  {
    s = s.substring(0,15);
  }
  // clean row before string
  for (int i = 0; i < ((OLED_SCREEN_WIDTH - s.length())/2); i++)
  {
    oled.setTextXY(iRow, i);
    oled.putChar(' '); 
  }
  // put string centered
  oled.setTextXY(iRow,((OLED_SCREEN_WIDTH - s.length())/2));              // Set cursor position
  oled.putString(s);
  // clean row after string
  for (int i = ((OLED_SCREEN_WIDTH/2) + (s.length()/2)); i < OLED_SCREEN_WIDTH; i++)
  {
    oled.setTextXY(iRow, i);
    oled.putChar(' '); 
  }
}


void oled_show_row_free_heap_8x8x16(int iRow)
{
  char buf[17];
  String s1 = "free heap:";
  oled.setTextXY(iRow,0);              // Set cursor position
  oled.putString(s1);
  String s2 = (String)ESP.getFreeHeap();
  for (int i = s1.length(); i < (OLED_SCREEN_WIDTH - s2.length()); i++)
  {
    oled.putChar(' ');
  }
  oled.setTextXY(iRow,(OLED_SCREEN_WIDTH - (s2.length())));              // Set cursor position
  oled.putString(s2);
  //Serial.print("free heap:");
  //Serial.print(s2);
  //Serial.println(".");
}

void oled_show_row_ip_address_8x8x16(int iRow)
{
  oled.setTextXY(iRow,0);
  for (int i = 0; i < (OLED_SCREEN_WIDTH - WiFi.localIP().toString().length()); i++)
  {
    oled.putChar(' ');
  }
  //oled.setTextXY(iRow,(OLED_SCREEN_WIDTH - WiFi.localIP().toString().length()));      // not needed, at this point is the cursor already at the right position
  oled.putString(WiFi.localIP().toString());

}

void oled_show_row_ssid_8x8x16(int iRow)
{
  oled.setTextXY(iRow,0);
  for (int i = 0; i < (OLED_SCREEN_WIDTH - ((String)ssid).length()); i++)
  {
    oled.putChar(' ');
  }
  //oled.setTextXY(iRow,(OLED_SCREEN_WIDTH - WiFi.localIP().toString().length()));      // not needed, at this point is the cursor already at the right position
  oled.putString((String)ssid);

}

void oled_show_row_hostname_8x8x16(int iRow)
{
  char buf[17];
  String s1 = "host:";
  oled.setTextXY(iRow,0);              // Set cursor position
  oled.putString(s1);
  String s2 = ((String)WiFi.hostname()).substring(0,11);
  /*
  oled.setTextXY(iRow, ( (OLED_SCREEN_WIDTH - s1.length()) / 2 ) );              // Set cursor position centered for 7x5
  oled.putString( s1 );
  */
  for(int i = s1.length(); i < (OLED_SCREEN_WIDTH - s2.length()); i++)  // blank space between string without screen flickering
  {
    oled.putChar(' ');
  }
  oled.setTextXY(iRow,(OLED_SCREEN_WIDTH - s2.length()));              // Set cursor position rightbound
  oled.putString(s2);
}

void oled_show_row_mac_address_8x8x16(int iRow)
{
  String mac = WiFi.macAddress();
  mac.remove(2,1);
  mac.remove(7,1);
  mac.remove(12,1);
  mac.toLowerCase();
  oled.setTextXY(iRow,0);              // Set cursor position
  for (int i = 0; i < (OLED_SCREEN_WIDTH - mac.length()); i++)
  {
    oled.putChar(' ');
  }
  //oled.setTextXY(iRow,(OLED_SCREEN_WIDTH - mac.length())); // not needed, at this point is the cursor already at the right position   
  oled.putString(mac);    
}

void oled_show_row_temp_8x8x16(int iRow)
{
  char buf[17];
  String s1 = "temp:";
  oled.setTextXY(iRow,0);              // Set cursor position
  oled.putString(s1);
  sprintf(buf, "%.1f~C", fTemp);
  for(int i = s1.length(); i < (OLED_SCREEN_WIDTH - ((String)buf).length()); i++)  // blank space between string without screen flickering
  {
    oled.putChar(' ');
  }
  oled.setTextXY(iRow,(OLED_SCREEN_WIDTH - ((String)buf).length() ));              // Set cursor position rightbound
  oled.putString(buf);
}

void oled_show_row_humi_8x8x16(int iRow)
{
  char buf[17];
  String s1 = "rHum:";
  oled.setTextXY(iRow,0);              // Set cursor position
  oled.putString(s1);
  sprintf(buf, "%.1f%%", fHum);
  for(int i = s1.length(); i < (OLED_SCREEN_WIDTH - ((String)buf).length()); i++)  // blank space between string without screen flickering
  {
    oled.putChar(' ');
  }
  oled.setTextXY(iRow,(OLED_SCREEN_WIDTH - ((String)buf).length() ));              // Set cursor position rightbound
  oled.putString(buf);
}

void oled_show_row_pres_8x8x16(int iRow)
{
  char buf[17];
  String s1 = "Pres:";
  oled.setTextXY(iRow,0);              // Set cursor position
  oled.putString(s1);
  sprintf(buf, "%.1fhPa", fPres);
  for(int i = s1.length(); i < (OLED_SCREEN_WIDTH - ((String)buf).length()); i++)  // blank space between string without screen flickering
  {
    oled.putChar(' ');
  }
  oled.setTextXY(iRow,(OLED_SCREEN_WIDTH - ((String)buf).length() ));              // Set cursor position rightbound
  oled.putString(buf);
}

void oled_show_row_uptime_8x8x16(int iRow)
{
    
  String s1 = "up:";
  String s2 = getUptime();
  oled.setTextXY(iRow,0);              // Set cursor position
  oled.putString(s1);
  for(int i = s1.length(); i < (OLED_SCREEN_WIDTH - s2.length()); i++)  // blank space between string without screen flickering
  {
    oled.putChar(' ');
  }
  oled.setTextXY(iRow,(OLED_SCREEN_WIDTH - s2.length()));              // Set cursor position rightbound
  oled.putString(s2);
}


void oled_show_row_rssi_8x8x16(int iRow)
{
    char buf[17];
    String s1 = "rssi:";
    oled.setTextXY(iRow,0);              // Set cursor position
    oled.putString(s1);
    sprintf(buf, "%ddBm", WiFi.RSSI());
    for(int i = s1.length(); i < (OLED_SCREEN_WIDTH - ((String)buf).length()); i++)  // blank space between string without screen flickering
    {
      oled.putChar(' ');
    }
    oled.setTextXY(iRow,(OLED_SCREEN_WIDTH - ((String)buf).length() ));              // Set cursor position rightbound
    oled.putString(buf);  
}

void bme280updateGlobalVars()
{
  delay(10); // wait to free the bus
  //bme280 specs: wait at least 1 second between two measurements
  //abs(): code works also on millis() overroll
  if ( abs( millis() - bme_last_read ) > 1100  )
  {
    float temp(NAN), hum(NAN), pres(NAN);
    BME280::TempUnit tempUnit(BME280::TempUnit_Celsius);
    BME280::PresUnit presUnit(BME280::PresUnit_Pa);
    bme.read(pres, temp, hum, tempUnit, presUnit);

    /* sometimes bme read fails and the variables stays at NAN so update global vars only if the value readings are valid 
     *  for example, if you want to trigger an led on, at the same pin where the bme280 sits with the data pin *facepalm*
     */ 
    if ( ( temp >= -40.0 ) && ( temp <= 85.0) )
    {
      fTemp = temp;
      fTemp = ((int)(fTemp*10))/10.0; // -> nur eine Nachkommastelle, Nachkommastellen des Divisior geben Nachkommastellen des Ergebnis an  
      //Serial.println("T");
    }
    else
    {
      Serial.print("temp read no valid response: ");
      Serial.println(temp);
    }

    if ( ( hum >= 0.0 ) && ( hum <= 100.0) )
    {
      fHum = hum;
      fHum  = ((int)(fHum *10))/10.0; // -> nur eine Nachkommastelle, Nachkommastellen des Divisior geben Nachkommastellen des Ergebnis an
      //Serial.println("H");
    }
    else
    {
      Serial.print("rhum read no valid response: ");
      Serial.println(hum);
    }

    if ( ( pres >= 30000.0 ) && ( pres <= 110000.0) )
    {
      fPres = pres/100;
      fHum  = ((int)(fHum *10))/10.0; // -> nur eine Nachkommastelle, Nachkommastellen des Divisior geben Nachkommastellen des Ergebnis an
      //Serial.println("P");
    }
    else
    {
      Serial.print("pres read no valid response: ");
      Serial.println(pres);
    }

    bme_last_read = millis();
  }
}



String getUptime()
{
  int iYears = 0, iMonths = 0, iWeeks = 0, iDays = 0, iHours = 0, iMinutes = 0; 
  String sUptime = "";
  unsigned long lUptime = millis()/1000;  //uptime as ul in sec

  while(lUptime >= 31536000)  // 365*24*60*60
  {
    lUptime -= 31536000;
    iYears ++;
  }

  while(lUptime >= 2678400)   // 31*24*60*60
  {
    lUptime -= 2678400;
    iMonths ++;
  }
  while(lUptime >= 604800)    // 7*24*60*60
  {
    lUptime -= 604800;
    iWeeks ++;
  }
  while(lUptime >= 86400)     // 24*60*60
  {
    lUptime -= 86400;
    iDays ++;
  }
  while(lUptime >= 3600)      // 60*60
  {
    lUptime -= 3600;
    iHours ++; 
  }
  while(lUptime >= 60)
  {
    lUptime -= 60;
    iMinutes ++;
  }

  if(iYears > 0)
  {
    sUptime = (String)iYears + "y";  
  }
  if(iMonths > 0)
  {
    sUptime += (String)iMonths + "m";  
  }
  if(iWeeks > 0)
  {
    sUptime += (String)iWeeks + "w";  
  }
  if(iDays > 0)
  {
    sUptime += (String)iDays + "d";  
  }
  if(iHours > 0)
  {
    sUptime += (String)iHours + "H";  
  }
  if(iMinutes > 0)
  {
    sUptime += (String)iMinutes + "M";  
  }
  sUptime += (String)lUptime + "S";
  
  return sUptime;
}
