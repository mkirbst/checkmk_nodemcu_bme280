Here goes all stuff for the esp8266 firmware.\
My prototype uses a NodeMCU because one have not to worry about serial-usb \
adapter for programming and power supply. Just connect micro usb cable to \
NodeMCU und your computer and you are ready to go.\

What to do to flash the firmware: \
\
1.) Install latest arduino IDE \
2.) In the Arduino IDE use the board manager to add support for esp8266 nodemcu \
3.) Now connect NodeMCU to your Computer \

4.) ENTER WLAN SSID AND WLAN PASSWORD OF YOUR WIFI IN THE SKETCH !! \

5.) Flash the firmware. \
6.) Connect bme280 sensor and display (optional) \
7.) Watch your router iffor DHCP request from the NodeMCU \
8.) Use telnet client to check if everything is working. If you Connect per \
telnet (Check_MK agent uses TCP Port 6556 by default): \
\
telnet 192.168.123.123 6556 \
\
then you should see raw output like this (192.168.123.123 is a placeholder for the actual IP of YOUR NodeMCU):

```
<<<check_mk>>>
Version: nodemcu_mkirbst_20190910
AgentOS: Arduino
Hostname: ESP_543210
OnlyFrom:
<<<uptime>>>
273.00 136.00
<<<nodemcu_mkirbst>>>
free_heap 44264 b
heap_frag 1 %
max_free_block_size 43520 b
Vcc 3.21 V
last_restart_reason External System
chip_id 01234567
core_version 2_5_2
sdk_version 2.2.1(cfd48f3)
cpu_freq 80 MHz
sketch_size 301360 b
free_sketch_space 200704 b
sketch_md5 ca29f64db48b6b805a548fb664d142fb
flash_chip_id 6543210
flash_chip_size 1048576
flash_chip_real_size 1048576
flash_chip_speed 40000000 Hz
cycle_count 456959688
rssi -61 dBm
mac 18:FE:34:54:32:10
hostname ESP_543210
local_ip_v4 192.168.178.132
gateway_v4 192.168.178.1
dns0_v4 192.168.178.1
dns1_v4 (IP unset)
dns2_v4 (IP unset)
dns3_v4 (IP unset)
ssid chucknorris
bssid 64:70:02:01:23:34
bme280 1 temp 27.6 °C
bme280 1 rhum 33 %
bme280 1 pres 998 hPa
bme280 1 value_age 659 ms
```


If you get raw output looking like above, you can continue to the checkmk folder in this repo. Your sensor node serves checkmk output now :-)

TROUBLESHOOTING: If the NodeMCU can not connect to the display and/or sensor, check the addresses and the wiring.