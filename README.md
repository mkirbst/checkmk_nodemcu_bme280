# checkmk_nodemcu_bme280

!!! WARNING: hobby project for fun - expect bad code "quality" !!!

The aim of this project is to build a temp/humidity/air pressure sensor node for \
check_mk for under 10 bucks. The hardware list is simple: A esp8266 (eg nodemcu 1.0), \
a Bosch BME280 sensor and for the eye candy an 128x64 pixel oled display. \
All this driven by an usb charger. 

HARDWARE:
- ESP8266 NodeMCU 1.0: [IZOKEE NodeMCU Module ESP8266 ESP-12E](https://www.amazon.de/gp/product/B01N4OYOKD/ref=ppx_yo_dt_b_search_asin_title?ie=UTF8&psc=1)
- Bosch [BME280](https://www.bosch-sensortec.com/products/environmental-sensors/humidity-sensors-bme280/) tempreature, relative humidity and air pressure sensor with i2c and spi interface
- Display [128 x 64 Pixel 0,96 Zoll OLED I2C Display](https://www.amazon.de/gp/product/B074N9VLZX/ref=ppx_yo_dt_b_search_asin_title?ie=UTF8&psc=1) (optional)

Hint: maybe it work with other hardware, but i have only tried it with the stuff listed above.

WIRING:

```
[NodeMCU 1.0]--[Bosch BME280]--[SSD1306 OLED]\
 GND-------------GND-------------GND
 Vcc +3.3V-------Vcc-------------Vcc
 D2--------------SDA-------------SDA
 D3--------------SCL-------------SCL
```


Hint: Don't use the buildin LED, this can harm interference with bus access.

Pictures:

In Check_MK 1.6 Raw Edition, output looks like:\
![](pictures/checkmk_nodemcu-checkmkoutput.png)
\
The hardware prototype (no housing, hold together with only one screw :-D):\
![](pictures/checkmk_nodemcu-prototype.jpg)

Warning: this is an hobby project, which was copy-pasted from snippets allover the\
internet. Code quality sucks, but as this project is just for fun, this is ok\
for me.

Known bugs:
- uptime counter overflow after 45 days
- expect a lot of more bugs :-P